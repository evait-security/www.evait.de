(function($) {          
    $(document).ready(function(){  
        var typewriter = new Typewriter(document.getElementById('typewriter'), {
            loop: false,
            delay: 300,
        });
        typewriter.typeString('hackers.');
        typewriter.start();                  
    });
})(jQuery);
